<?php
namespace CsTest\Unit\Stdlib\Hydrator\Filter;

use Cs\Stdlib\Hydrator\Filter\PropertyNameBlacklistFilter;

class PropertyNameBlacklistFilterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Tests that any property name returns TRUE when no property name list is passed.
     * @return void
     *
     * @test
     */
    public function emptyPropertyNameList()
    {
        $filter = new PropertyNameBlacklistFilter();
        $this->assertTrue($filter->filter('some.property.name'));
    }

    /**
     * [simpleCreate description]
     * @return void
     *
     * @test
     * @dataProvider provideSimpleFilter
     */
    public function simpleFilter($list, $property, $expected)
    {
        $filter = new PropertyNameBlacklistFilter($list);
        $this->assertSame($expected, $filter->filter($property));
    }

    public function provideSimpleFilter()
    {
        $propertyClassPrefix = __CLASS__ . '::';

        $list = [
            'getFullName',
            'getFullTitle',
        ];

        return [
            [$list, $propertyClassPrefix . 'getFullName', false],
            [$list, $propertyClassPrefix . 'getFullTitle', false],
            [$list, $propertyClassPrefix . 'doSomething', true],
            [$list, $propertyClassPrefix . 'getFullNameDifferently', true],
            [$list, $propertyClassPrefix . 'getFullTitleDifferently', true],
        ];
    }
}
