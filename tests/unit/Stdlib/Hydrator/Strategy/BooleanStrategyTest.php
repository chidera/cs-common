<?php
namespace CsTest\Unit\Stdlib\Hydrator\Strategy;

use Cs\Stdlib\Hydrator\Strategy\BooleanStrategy;

class BooleanStrategyTest extends \PHPUnit_Framework_TestCase
{
    private $strategy = null;

    public function setUp()
    {
        $this->strategy = new BooleanStrategy();
    }

    /**
     * Tests that extract() works as expected.
     * @return void
     *
     * @test
     * @dataProvider provideExtract
     */
    public function extract($value, $expect)
    {
        $this->assertSame($expect, $this->strategy->extract($value));
    }

    public function provideExtract()
    {
        return [
            [null, 0],
            [true, 1],
            [false, 0],
        ];
    }

    /**
     * Tests that hydrate() works as expected.
     * @return void
     *
     * @test
     * @dataProvider provideHydrate
     */
    public function hydrate($value, $expect)
    {
        $this->assertSame($expect, $this->strategy->hydrate($value));
    }

    public function provideHydrate()
    {
        return [
            [null, false],
            [0, false],
            [1, true],
            [9, true],
            [-1, false],
        ];
    }
}
