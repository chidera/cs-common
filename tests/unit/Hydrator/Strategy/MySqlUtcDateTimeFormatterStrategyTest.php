<?php
namespace CsTest\Unit\Hydrator\Strategy;

use Cs\Hydrator\Strategy\MySqlUtcDateTimeFormatterStrategy;
use PHPUnit_Framework_TestCase;
use DateTime;

class MySqlUtcDateTimeFormatterStrategyTest extends PHPUnit_Framework_TestCase
{
    private $strategy = null;

    public function setUp()
    {
        $this->strategy = new MySqlUtcDateTimeFormatterStrategy();
    }

    /**
     * Tests that extract() works as expected.
     * @return void
     *
     * @test
     * @dataProvider provideExtract
     */
    public function extract($value, $expect)
    {
        $this->assertSame($expect, $this->strategy->extract($value));
    }

    public function provideExtract()
    {
        return [
            [null, null],
            [new DateTime('1990-08-07 12:34:56'), '1990-08-07 12:34:56'],
        ];
    }

    /**
     * Tests that hydrate() works as expected when given null values.
     * @return void
     *
     * @test
     */
    public function hydrateNull()
    {
        $this->assertNull($this->strategy->hydrate(null));
    }

    /**
     * Tests that hydrate() works as expected when given non-null values.
     * @return void
     *
     * @test
     * @dataProvider provideHydrateNonNull
     */
    public function hydrateNonNull($data, $expected)
    {
        $this->assertSame($expected, $this->strategy->hydrate($data)->format('Y-m-d H:i:s'));
    }

    public function provideHydrateNonNull()
    {
        return [
            ['1990-08-07 12:34:56', '1990-08-07 12:34:56'],
        ];
    }
}
