<?php
namespace CsTest\Unit\Core;

use Cs\Core\BaseEntityTrait;
use DateTime;

/**
 * It should be noted that most of these tests are simply re-testing what is
 * already done in the trait's "sub-traits", but since it does not seem
 * possible to verify that such "sub-traits" are present, we are left to re-
 * testing.
 */
class BaseEntityTraitTest extends \PHPUnit_Framework_TestCase
{
    protected $baseEntity;

    public function setUp()
    {
        $this->baseEntity = $this->getObjectForTrait(BaseEntityTrait::class);
    }

    /**
     * @test
     */
    public function hasCorrectDefaultPropertyValues()
    {
        $this->assertNull($this->baseEntity->getId());
        $this->assertNull($this->baseEntity->getDateTimeCreated());
        $this->assertNull($this->baseEntity->getDateTimeLastUpdated());
    }

    /**
     * @test
     */
    public function hasFluentInterface()
    {
        $this->assertSame($this->baseEntity, $this->baseEntity->setId(1234));
        $this->assertSame($this->baseEntity, $this->baseEntity->setDateTimeCreated(new DateTime()));
        $this->assertSame($this->baseEntity, $this->baseEntity->setDateTimeLastUpdated(new DateTime()));
    }

    /**
     * @test
     */
    public function gettersSettersWork()
    {
        $id = 1234;
        $dateTimeCreated = new DateTime();
        $dateTimeLastUpdated = new DateTime();

        $this->baseEntity->setId($id);
        $this->baseEntity->setDateTimeCreated($dateTimeCreated);
        $this->baseEntity->setDateTimeLastUpdated($dateTimeLastUpdated);

        $this->assertSame($id, $this->baseEntity->getId());
        $this->assertSame($dateTimeCreated, $this->baseEntity->getDateTimeCreated());
        $this->assertSame($dateTimeLastUpdated, $this->baseEntity->getDateTimeLastUpdated());
    }

    /**
     * @test
     * @expectedException Exception
     */
    public function cannotSetIdOnceSet()
    {
        $this->baseEntity->setId(1234);
        $this->baseEntity->setId(1234);
    }

    /**
     * @test
     * @expectedException Exception
     */
    public function settingNonNullDateTimeCreatedThrows()
    {
        $dateTime = new DateTime();

        $this->baseEntity->setDateTimeCreated($dateTime);
        $this->baseEntity->setDateTimeCreated($dateTime);
    }
}
