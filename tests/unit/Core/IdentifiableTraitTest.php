<?php
namespace CsTest\Unit\Core;

class IdentifiableTraitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function isInitiallyNull()
    {
        $identifiable = $this->getObjectForTrait('Cs\Core\IdentifiableTrait');

        $this->assertNull($identifiable->getId());

        return $identifiable;
    }

    /**
     * @depends isInitiallyNull
     * @test
     */
    public function settingIdWorksAndIsFluent($identifiable)
    {
        $id = 1234;

        $return = $identifiable->setId($id);

        $this->assertSame($id, $identifiable->getId());
        $this->assertSame($return, $identifiable);

        return $identifiable;
    }

    /**
     * @depends settingIdWorksAndIsFluent
     * @expectedException Exception
     * @test
     */
    public function settingNonNullIdThrows($identifiable)
    {
        $id = 4321;

        $this->assertNotNull($identifiable->getId());

        $identifiable->setId($id);
    }
}
