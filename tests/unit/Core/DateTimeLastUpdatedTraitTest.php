<?php
namespace CsTest\Unit\Core;

use Cs\Core\DateTimeLastUpdatedTrait;
use DateTime;

class DateTimeLastUpdatedTraitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function isInitiallyNull()
    {
        $dateTimeLastUpdated = $this->getObjectForTrait(DateTimeLastUpdatedTrait::class);

        $this->assertNull($dateTimeLastUpdated->getDateTimeLastUpdated());

        return $dateTimeLastUpdated;
    }

    /**
     * @test
     * @depends isInitiallyNull
     */
    public function settingDateTimeLastUpdatedWorksAndIsFluent($dateTimeLastUpdated)
    {
        $dateTime = new DateTime();

        $return = $dateTimeLastUpdated->setDateTimeLastUpdated($dateTime);

        $this->assertSame($dateTime, $dateTimeLastUpdated->getDateTimeLastUpdated());
        $this->assertSame($return, $dateTimeLastUpdated);

        return $dateTimeLastUpdated;
    }

    /**
     * @test
     * @depends settingDateTimeLastUpdatedWorksAndIsFluent
     */
    public function canSetDateTimeLastUpdatedOnceSet($dateTimeLastUpdated)
    {
        $initialDateTime = $dateTimeLastUpdated->getDateTimeLastUpdated();

        $this->assertNotNull($dateTimeLastUpdated->getDateTimeLastUpdated());

        $newDateTime = new DateTime();

        $dateTimeLastUpdated->setDateTimeLastUpdated($newDateTime);

        $this->assertNotSame($initialDateTime, $dateTimeLastUpdated->getDateTimeLastUpdated());
        $this->assertSame($newDateTime, $dateTimeLastUpdated->getDateTimeLastUpdated());
    }
}
