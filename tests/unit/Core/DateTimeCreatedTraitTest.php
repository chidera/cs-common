<?php
namespace CsTest\Unit\Core;

use Cs\Core\DateTimeCreatedTrait;
use DateTime;

class DateTimeCreatedTraitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function isInitiallyNull()
    {
        $dateTimeCreated = $this->getObjectForTrait(DateTimeCreatedTrait::class);

        $this->assertNull($dateTimeCreated->getDateTimeCreated());

        return $dateTimeCreated;
    }

    /**
     * @test
     * @depends isInitiallyNull
     */
    public function settingDateTimeCreatedWorksAndIsFluent($dateTimeCreated)
    {
        $dateTime = new DateTime();

        $return = $dateTimeCreated->setDateTimeCreated($dateTime);

        $this->assertSame($dateTime, $dateTimeCreated->getDateTimeCreated());
        $this->assertSame($return, $dateTimeCreated);

        return $dateTimeCreated;
    }

    /**
     * @test
     * @depends settingDateTimeCreatedWorksAndIsFluent
     * @expectedException Exception
     */
    public function settingNonNullDateTimeCreatedThrows($dateTimeCreated)
    {
        $dateTime = new DateTime();

        $this->assertNotNull($dateTimeCreated->getDateTimeCreated());

        $dateTimeCreated->setDateTimeCreated($dateTime);
    }
}
