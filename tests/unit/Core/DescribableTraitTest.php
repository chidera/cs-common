<?php
declare(strict_types=1);

namespace CsTest\Unit\Core;

use Cs\Core\DescribableTrait;

class DescribableTraitTest extends \PHPUnit_Framework_TestCase
{
    private $describable = null;

    public function setUp()
    {
        $this->describable = $this->getObjectForTrait(DescribableTrait::class);
    }

    /**
     * @test
     */
    public function isInitiallyEmptyString()
    {
        $this->assertSame('', $this->describable->getDescription());
    }

    /**
     * @test
     */
    public function setterWorksAndIsFluent()
    {
        $description = 'some.description';

        $return = $this->describable->setDescription($description);

        $this->assertSame($description, $this->describable->getDescription());
        $this->assertSame($return, $this->describable);
    }
}
