<?php
namespace CsTest\Unit\Core;

class EmailAddressTraitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function isInitiallyNull()
    {
        $emailAddressTraitObject = $this->getObjectForTrait('Cs\Core\EmailAddressTrait');

        $this->assertNull($emailAddressTraitObject->getEmailAddress());

        return $emailAddressTraitObject;
    }

    /**
     * @depends isInitiallyNull
     * @test
     */
    public function settingIdWorksAndIsFluent($emailAddressTraitObject)
    {
        $emailAddress = 'some.email@address.com';

        $return = $emailAddressTraitObject->setEmailAddress($emailAddress);

        $this->assertSame($emailAddress, $emailAddressTraitObject->getEmailAddress());
        $this->assertSame($return, $emailAddressTraitObject);

        return $emailAddressTraitObject;
    }
}
