<?php
namespace CsTest\Unit\Core;

class NameableTraitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function isInitiallyNull()
    {
        $nameable = $this->getObjectForTrait('Cs\Core\NameableTrait');

        $this->assertNull($nameable->getName());

        return $nameable;
    }

    /**
     * @depends isInitiallyNull
     * @test
     */
    public function settingIdWorksAndIsFluent($nameable)
    {
        $name = 'some.name';

        $return = $nameable->setName($name);

        $this->assertSame($name, $nameable->getName());
        $this->assertSame($return, $nameable);
    }
}
