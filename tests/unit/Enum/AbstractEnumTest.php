<?php
namespace CsTest\Unit\Enum;

class AbstractEnumTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider providerValidValues
     */
    public function testValidValues($value)
    {
        $this->assertTrue(TestAsset\SimpleEnum::isValid($value));
    }

    /**
     * @dataProvider providerInvalidValues
     */
    public function testInvalidValues($value)
    {
        $this->assertFalse(TestAsset\SimpleEnum::isValid($value));
    }

    public function providerValidValues()
    {
        return array(
            //    $value
            array(TestAsset\SimpleEnum::INTEGER_CONST), // Direct, explicit use of all supported constants...
            array(TestAsset\SimpleEnum::BOOLEAN_CONST),
            array(TestAsset\SimpleEnum::FLOAT_CONST),
            array(TestAsset\SimpleEnum::STRING_CONST),
        );
    }

    /**
     * NOTE: This list is a list of obvious known invalid values, some close to
     * the real ones, some not.  If any of these fail, then a first check should
     * be to make sure that none of the values have changed and that these are,
     * indeed, invalid values.
     *
     * @return type
     */
    public function providerInvalidValues()
    {
        return array(
            //    $value
            array(null),
            array(0),
            array(4321),
            array(-1234),
            array(false),
            array('true'),
            array(43.21),
            array(-12.34),
            array(''),
            array(' '),
            array('TestAsset\SimpleEnum::STRING_CONST'),
            array(' ' . TestAsset\SimpleEnum::STRING_CONST),
            array(TestAsset\SimpleEnum::STRING_CONST . ' '),
            array(' ' . TestAsset\SimpleEnum::STRING_CONST . ' '),
        );
    }
}
