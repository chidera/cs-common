<?php
namespace CsTest\Unit\Enum\TestAsset;

use Cs\Enum\AbstractEnum;

class SimpleEnum extends AbstractEnum
{
    const INTEGER_CONST = 1234;
    const BOOLEAN_CONST = true;
    const FLOAT_CONST = 12.34;
    const STRING_CONST = 'random.string.content';
}
