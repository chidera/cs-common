<?php
namespace CsTest\Unit\Db;

use Cs\Db\TableGatewayTrait;
use Mockery as m;
use Zend\Db\TableGateway\TableGatewayInterface;

class TableGatewayTraitTest extends \PHPUnit_Framework_TestCase
{
    private $tableGatewayTrait;

    public function setUp()
    {
        $this->tableGatewayTrait = $this->getObjectForTrait(TableGatewayTrait::class);
    }

    /**
     * @test
     */
    public function isInitiallyNull()
    {
        $this->assertNull($this->tableGatewayTrait->getTableGateway());
    }

    /**
     * @test
     */
    public function settingTableGatewayWorksAndIsFluent()
    {
        $tableGateway = m::mock(TableGatewayInterface::class);

        $return = $this->tableGatewayTrait->setTableGateway($tableGateway);

        $this->assertSame($tableGateway, $this->tableGatewayTrait->getTableGateway());
        $this->assertSame($return, $this->tableGatewayTrait);
    }
}
