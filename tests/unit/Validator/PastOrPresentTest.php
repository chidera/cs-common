<?php
namespace CsTest\Unit\Validator;

use Cs\Validator\PastOrPresent;
use CsDev\PhpUnit\DataProvider\FutureDateTimeStringProviderTrait;
use CsDev\PhpUnit\DataProvider\NonDateTimeProviderTrait;
use CsDev\PhpUnit\DataProvider\PastDateTimeStringProviderTrait;
use DateTime;
use StdClass;

class PastOrPresentTest extends \PHPUnit_Framework_TestCase
{
    use FutureDateTimeStringProviderTrait;

    use NonDateTimeProviderTrait;

    use PastDateTimeStringProviderTrait;

    private $validator;

    public function setUp()
    {
        $this->validator = new PastOrPresent();
    }

    /**
     * Tests that NULL returns FALSE.
     *
     * @return void
     *
     * @test
     */
    public function null()
    {
        $this->assertFalse($this->validator->isValid(null));
    }

    /**
     * Tests that non-DateTime input returns FALSE.
     *
     * @return void
     *
     * @test
     * @dataProvider nonDateTimeProvider
     */
    public function nonDateTime($input)
    {
        $this->assertFalse($this->validator->isValid($input));
    }

    /**
     * Tests that past DateTime input returns TRUE.
     *
     * @return void
     *
     * @test
     * @dataProvider pastDateTimeStringProvider
     */
    public function pastDateTime($input)
    {
        $this->assertTrue($this->validator->isValid(new DateTime($input)));
    }

    /**
     * Tests that present DateTime input returns TRUE.
     *
     * @return void
     *
     * @test
     */
    public function presentDateTime()
    {
        $this->assertTrue($this->validator->isValid(new DateTime()));
    }

    /**
     * Tests that future DateTime input returns FALSE.
     *
     * @return void
     *
     * @test
     * @dataProvider futureDateTimeStringProvider
     */
    public function futureDateTime($input)
    {
        $this->assertFalse($this->validator->isValid(new DateTime($input)));
    }
}
