<?php
namespace CsTest\Unit\DateTime;

use Cs\DateTime\DateTimeProvider;
use phpunit\framework\TestCase;
use DateTime;

class DateTimeProviderTest extends TestCase
{
    private $dateTimeProvider = null;

    public function setUp()
    {
        $this->dateTimeProvider = new DateTimeProvider();
    }

    /**
     * Tests that now() returns a DateTime object.
     * @return void
     *
     * @test
     */
    public function now()
    {
        $this->assertInstanceOf(DateTime::class, $this->dateTimeProvider->now());
    }
}
