all: clean test

clean:
	@./vendor/bin/php-cs-fixer fix -v

clean-dry:
	@./vendor/bin/php-cs-fixer fix --dry-run -v #--diff

test:
	@./vendor/bin/phpunit -c ./tests/phpunit.xml
