<?php
namespace Cs\Enum;

use ReflectionClass;

abstract class AbstractEnum
{
    /**
     *
     * @param  type $value
     * @return type
     * @todo Consider changing this to a trait, if possible...
     */
    public static function isValid($value)
    {
        $class = new ReflectionClass(get_called_class());
        $classConstants = $class->getConstants();
        return in_array($value, $classConstants, true);
    }
}
