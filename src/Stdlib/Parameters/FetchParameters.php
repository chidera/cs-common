<?php
namespace Cs\Stdlib\Parameters;

use Zend\Stdlib\Parameters as StdlibParams;
use Cs\Core\SortOrder;

/**
 * @todo Need to allow properties to be set, too.
 */
class FetchParameters
{
    const RPP_PARAM = 'rpp';

    const PAGE_PARAM = 'page';

    const SORT_ORDER_PARAM = 'order';

    const SORT_FIELD_PARAM = 'sort';

    const KEYWORD_SEARCH_PARAM = 'q';

    const DEFAULT_RPP = 10;

    const DEFAULT_PAGE = 1;

    const DEFAULT_SORT_ORDER = SortOrder::ASCENDING;

    const DEFAULT_SORT_FIELD = '';

    const DEFAULT_KEYWORDS = '';

    private $recordsPerPage = self::DEFAULT_RPP;
    private $page = self::DEFAULT_PAGE;
    private $sortOrder = self::DEFAULT_SORT_ORDER;
    private $sortField = self::DEFAULT_SORT_FIELD;
    private $keywords = self::DEFAULT_KEYWORDS;

    /**
     * @todo Need to allow constructor to be instantiated *without* parameters.
     */
    public function __construct(StdlibParams $params)
    {
        $this->recordsPerPage = $params->get(self::RPP_PARAM, self::DEFAULT_RPP);
        $this->page = $params->get(self::PAGE_PARAM, self::DEFAULT_PAGE);
        $this->sortOrder = $params->get(self::SORT_ORDER_PARAM, self::DEFAULT_SORT_ORDER);
        $this->sortField = $params->get(self::SORT_FIELD_PARAM, self::DEFAULT_SORT_FIELD);
        $this->keywords = $params->get(self::KEYWORD_SEARCH_PARAM, self::DEFAULT_KEYWORDS);
    }

    public function getRecordsPerPage()
    {
        return $this->recordsPerPage;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    public function getSortField()
    {
        return $this->sortField;
    }

    /**
     *
     * @return array
     *
     * @todo Is this the right place for such functionality...?
     */
    public function getKeywordsAsArray()
    {
        return explode(' ', $this->keywords);
    }
}
