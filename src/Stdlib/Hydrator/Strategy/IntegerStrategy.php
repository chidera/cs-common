<?php
namespace Cs\Stdlib\Hydrator\Strategy;

use Zend\Hydrator\Strategy\StrategyInterface;

/**
 * @deprecated Use Cs\Hydrator\Strategy\IntegerStrategy instead.
 */
class IntegerStrategy implements StrategyInterface
{
    /**
     * Converts the given value so that it can be extracted by the hydrator.
     *
     * @param  mixed $value The original value.
     * @return mixed Returns the value that should be extracted.
     */
    public function extract($value)
    {
        return (int) $value;
    }

    /**
     * Converts the given value so that it can be hydrated by the hydrator.
     *
     * @param  mixed $value The original value.
     * @return mixed Returns the value that should be hydrated.
     */
    public function hydrate($value)
    {
        return (int) $value;
    }
}
