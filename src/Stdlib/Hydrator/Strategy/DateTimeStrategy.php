<?php
namespace Cs\Stdlib\Hydrator\Strategy;

use DateTime;

/**
 * @deprecated Use Zend\Hydrator\Strategy\DateTimeFormatterStrategy from zendframework/zend-hydrator or Cs\Hydrator\Strategy\MySqlUtcDateTimeFormatterStrategy instead.
 */
class DateTimeStrategy extends DateStrategy
{
    /**
     * Converts the given value so that it can be extracted by the hydrator.
     *
     * @param  mixed $value The original value.
     * @return mixed Returns the value that should be extracted.
     */
    public function extract($value)
    {
        if (null === $value) {
            return $value;
        }

        return $value->format('Y-m-d H:i:s');
    }

    /**
     * Converts the given value so that it can be hydrated by the hydrator.
     *
     * @param  mixed $value The original value.
     * @return mixed Returns the value that should be hydrated.
     */
    public function hydrate($value)
    {
        if (null === $value) {
            return $value;
        }

        return new DateTime($value);
    }
}
