<?php
namespace Cs\Stdlib\Hydrator\Strategy;

use Zend\Hydrator\Strategy\StrategyInterface;

/**
 * @deprecated Use Zend\Hydrator\Strategy\BooleanStrategy from zendframework/zend-hydrator instead.
 */
class BooleanStrategy implements StrategyInterface
{
    /**
     * Converts the given value so that it can be extracted by the hydrator.
     *
     * @param  mixed $value The original value.
     * @return mixed Returns the value that should be extracted.
     */
    public function extract($value)
    {
        return ($value ? 1 : 0);
    }

    /**
     * Converts the given value so that it can be hydrated by the hydrator.
     *
     * @param  mixed $value The original value.
     * @return mixed Returns the value that should be hydrated.
     */
    public function hydrate($value)
    {
        return ($value > 0);
    }
}
