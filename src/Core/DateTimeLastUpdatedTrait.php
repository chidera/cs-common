<?php
namespace Cs\Core;

use DateTime;

trait DateTimeLastUpdatedTrait
{
    /**
     *
     * @var DateTime
     */
    private $dateTimeLastUpdated = null;

    /**
     *
     * @return DateTime
     */
    public function getDateTimeLastUpdated()
    {
        return $this->dateTimeLastUpdated;
    }

    /**
     *
     * @param  DateTime   $value
     * @return mixed
     * @throws \Exception
     *
     * @todo Should this be editable once set, like with DateTimeCreated...?
     */
    public function setDateTimeLastUpdated(DateTime $value = null)
    {
        $this->dateTimeLastUpdated = $value;
        return $this;
    }
}
