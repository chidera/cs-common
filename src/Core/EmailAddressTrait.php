<?php
namespace Cs\Core;

trait EmailAddressTrait
{
    private $emailAddress = null;

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
        return $this;
    }
}
