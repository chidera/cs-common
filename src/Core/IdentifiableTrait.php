<?php
namespace Cs\Core;

trait IdentifiableTrait
{
    private $id = null;

    public function getId()
    {
        return $this->id;
    }

    public function setId($value)
    {
        if (null === $this->id) {
            $this->id = $value;
            return $this;
        }
        throw new \Exception('An entity ID may not be changed once set');
    }
}
