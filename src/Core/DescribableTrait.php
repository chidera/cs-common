<?php
declare(strict_types=1);

namespace Cs\Core;

trait DescribableTrait
{
    private $description = '';

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $value)
    {
        $this->description = $value;
        return $this;
    }
}
