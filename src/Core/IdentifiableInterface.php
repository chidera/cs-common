<?php
namespace Cs\Core;

interface IdentifiableInterface
{
    public function getId();

    public function setId($value);
}
