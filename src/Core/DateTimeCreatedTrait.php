<?php
namespace Cs\Core;

use DateTime;

trait DateTimeCreatedTrait
{
    /**
     *
     * @var DateTime
     */
    private $dateTimeCreated = null;

    /**
     *
     * @return DateTime
     */
    public function getDateTimeCreated()
    {
        return $this->dateTimeCreated;
    }

    /**
     *
     * @param  DateTime   $dateTimeCreated
     * @return mixed
     * @throws \Exception
     */
    public function setDateTimeCreated(DateTime $dateTimeCreated)
    {
        if (null === $this->dateTimeCreated) {
            $this->dateTimeCreated = $dateTimeCreated;
            return $this;
        }
        throw new \Exception('An entity\'s dateTimeCreated property may not be changed once set');
    }
}
