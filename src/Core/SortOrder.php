<?php
namespace Cs\Core;

class SortOrder
{
    const ASCENDING = 'asc';
    const DESCENDING = 'desc';
}
