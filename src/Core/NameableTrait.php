<?php
namespace Cs\Core;

trait NameableTrait
{
    private $name = null;

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }
}
