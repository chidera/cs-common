<?php
namespace Cs\Core;

trait BaseEntityTrait
{
    use IdentifiableTrait;

    use DateTimeCreatedTrait;

    use DateTimeLastUpdatedTrait;
}
