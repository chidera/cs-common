<?php
namespace Cs\Validator;

use Zend\Validator\AbstractValidator;
use DateTime;

/**
 * Validates that a date/time is in the past or the present, not in the future.
 *
 * @todo Consider allowing a user to modify "variance", or "grace", options that allow "now" to be within a specified range to account for different precision contexts as well as issues with getting "now" at different times during execution.
 * @todo Consider allowing the value to be either a DateTime object or a string.
 * @todo Consider allowing a user to modify/specify "now".
 */
class PastOrPresent extends AbstractValidator
{
    const DATE_TIME = 'dateTime';
    const NULL = 'null';
    const FUTURE  = 'future';

    protected $messageTemplates = [
        self::DATE_TIME => "Input must be of type DateTime",
        self::NULL => "Input must not be NULL",
        self::FUTURE  => "Input must not be in the future",
    ];

    public function isValid($value)
    {
        $this->setValue($value);

        if (!($value instanceof DateTime)) {
            $this->error(self::DATE_TIME);
            return false;
        }

        if (null === $value) {
            $this->error(self::NULL);
            return false;
        }

        if ((new DateTime()) < $value) {
            $this->error(self::FUTURE);
            return false;
        }

        return true;
    }
}
