<?php
namespace Cs\DateTime;

use DateTime;

class DateTimeProvider
{
    /**
     * 
     * @return DateTime
     */
    public function now()
    {
        return new DateTime();
    }
}
