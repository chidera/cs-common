<?php
namespace Cs\Hydrator\Filter;

use Zend\Hydrator\Filter\FilterInterface;

class PropertyNameBlacklistFilter implements FilterInterface
{
    private $propertyNames = null;

    public function __construct(array $propertyNames = [])
    {
        $this->propertyNames = $propertyNames;
    }

    public function filter($property)
    {
        $pos = strpos($property, '::');
        if ($pos !== false) {
            $pos += 2;
        } else {
            $pos = 0;
        }

        return (!in_array(substr($property, $pos), $this->propertyNames));
    }
}
