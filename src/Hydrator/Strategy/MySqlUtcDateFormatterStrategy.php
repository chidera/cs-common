<?php
namespace Cs\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DateTimeFormatterStrategy;
use Zend\Hydrator\Strategy\StrategyInterface;
use DateTime;
use DateTimeZone;

class MySqlUtcDateFormatterStrategy implements StrategyInterface
{
    private $dateTimeFormatterStrategy = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dateTimeFormatterStrategy = new DateTimeFormatterStrategy('Y-m-d', new DateTimeZone('UTC'));
    }

    /**
     * Converts to MySQL DATE string
     *
     * @param mixed|DateTime $value
     *
     * @return mixed|string
     */
    public function extract($value)
    {
        return $this->dateTimeFormatterStrategy->extract($value);
    }

    /**
     * Converts a MySQL DATE value to DateTime instance for injecting to object
     *
     * @param mixed|string $value
     *
     * @return mixed|DateTime
     */
    public function hydrate($value)
    {
        return $this->dateTimeFormatterStrategy->hydrate($value);
    }
}
