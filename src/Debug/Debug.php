<?php
namespace Cs\Debug;

class Debug extends \Zend\Debug\Debug
{
    public static function out($text, $newline = true)
    {
        echo $text;
        if ($newline) {
            echo PHP_EOL;
        }
    }
}
