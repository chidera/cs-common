<?php
namespace Cs\Db;

use Zend\Db\TableGateway\TableGatewayInterface;

trait TableGatewayTrait
{
    protected $tableGateway = null;

    /**
     * @return TableGatewayInterface
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    /**
     * 
     * @param TableGatewayInterface $value Table gateway
     * @return TableGatewayTrait
     */
    public function setTableGateway(TableGatewayInterface $value)
    {
        $this->tableGateway = $value;

        return $this;
    }
}
